package db;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MySqlDataBaseConnection {

    private static Connection conn;
    public static String SQL_SELECT = "SELECT * from wp_posts;";
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;

    public static Connection connectToDb() {
        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://192.168.247.5:3306/wordpress", "root", "password");
        } catch (SQLException e) {
            e.getMessage();
        }

        return conn;
    }

    public static ResultSet fetchResults() {
        connectToDb();
        try {
            preparedStatement = conn.prepareStatement(SQL_SELECT);
            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;

    }

    public static List<Map> getResultsAsMap(ResultSet rs) {
        List<Map> list = new ArrayList<Map>();
        try {
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();

            while (rs.next()) {
                HashMap row = new HashMap(columns);
                for (int i = 1; i <= columns; ++i) {
                    row.put(md.getColumnName(i), rs.getObject(i));
                }
                list.add(row);
            }
        } catch (SQLException e) {
            e.getMessage();
        }
        return list;
    }

    public static void main(String[] args) {
        getResultsAsMap(fetchResults());
    }


}
